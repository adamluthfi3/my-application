package com.example.gredu.myapplication.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.adapter.AccountAdapter
import com.example.gredu.myapplication.model.Account
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_thrid.*

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [thrid_fragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [thrid_fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class thrid_fragment : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_thrid, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupAppBarLayout()
        val listAccount = ArrayList<Account>()
        listAccount.add(Account(0,"Milea Hermansyah","SMA 7 Jakarta - X IPA 1"))
        listAccount.add(Account(0,"Chelsea Hermansyah","SMA 9 Jakarta - XI IPA 4"))
        listAccount.add(Account(0,"Anang Hermansyah","SMA 9 Jakarta - XI IPA 4"))

        val adapter = AccountAdapter(listAccount,activity,object : AccountItemClick{
            override fun onAccountItemClick() {
            }
        })

        listProfile!!.layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL,false)
        listProfile.adapter = adapter
    }

    fun setupAppBarLayout(){
        barLayout.addOnOffsetChangedListener{_, verticalOffset ->
            if (verticalOffset <= -linearProfile.height / 2) collapsingToolbar.title = "Profilku" else collapsingToolbar.title = ""
        }
        collapsingToolbar.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment thrid_fragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): thrid_fragment {
            val fragment = thrid_fragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor

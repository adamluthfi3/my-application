package com.example.gredu.myapplication

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import com.example.gredu.myapplication.adapter.UserAdapter
import com.example.gredu.myapplication.model.User
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val users = ArrayList<User>()

        users.add(User("adam","123455"))
        users.add(User("adam","123455"))
        users.add(User("adam","123455"))
        users.add(User("adam","123455"))

        val adapter = UserAdapter(users)

        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        recyclerView.adapter = adapter

        btnLogin.setOnClickListener{
            users.add(User("luthfi","34566"))
            adapter.notifyDataSetChanged()
        }

    }

}

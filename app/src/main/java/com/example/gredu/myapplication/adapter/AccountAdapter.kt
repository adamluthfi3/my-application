package com.example.gredu.myapplication.adapter

import android.content.Context
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.fragment.AccountItemClick
import com.example.gredu.myapplication.fragment.account_fragment
import com.example.gredu.myapplication.model.Account
import kotlinx.android.synthetic.main.item_account.view.*

/**
 * Created by gredu on 30/11/17.
 */
class AccountAdapter (val account: ArrayList<Account>,val context: Context,val itemCLick: AccountItemClick):RecyclerView.Adapter<AccountAdapter.ViewHolder>() {

    var isSelectedPosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder{
        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.item_account,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder!!.tvNameAccount.setText(account[position].name)
        holder.tvKetAccount.setText(account[position].ket)

        holder.radioButtonAccount.setChecked(position == isSelectedPosition)
        holder.radioButtonAccount.isChecked = true
        holder.radioButtonAccount.setTag(position)
        holder.radioButtonAccount.setOnClickListener{
            itemCheckChange(holder.radioButtonAccount)
            itemCLick.onAccountItemClick()
        }
    }

    override fun getItemCount(): Int {
        return account.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val imgListAccount = itemView.img_list_account
        val tvNameAccount = itemView.tv_name_account
        val tvKetAccount = itemView.tv_ket_account
        val radioButtonAccount = itemView.rb_list_account
    }

    fun itemCheckChange(v: View){
        isSelectedPosition = v.tag as Int
        notifyDataSetChanged()
    }




}
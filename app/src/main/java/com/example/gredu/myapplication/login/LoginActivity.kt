package com.example.gredu.myapplication.login

import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.gredu.myapplication.R
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_content.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)
        setSupportActionBar(tool_bar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        val nama: TextView = findViewById(R.id.etName)
        val name: String = nama.text.toString()
        val btnTest: Button = findViewById(R.id.btnTest)

        btnTest.setOnClickListener {
            if (name.equals("")){
                Toast.makeText(this,"Editext Kosong",Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this,"OK",Toast.LENGTH_SHORT).show()
            }
        }

        setupAppBarLayout()
        //setupList()
    }


    fun setupAppBarLayout(){
        appBarLayout.addOnOffsetChangedListener{_, verticalOffset ->
            if (verticalOffset <= -linearLayout.height / 2) collapsingToolbarLayout.title = "Profile" else collapsingToolbarLayout
                    .title = ""
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home){
            finish()
        }
            return super.onOptionsItemSelected(item)
    }
}

package com.example.gredu.myapplication.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.widget.LinearLayout

import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.adapter.ScheduleAdapter
import com.example.gredu.myapplication.adapter.collaps
import com.example.gredu.myapplication.model.Schedule
import kotlinx.android.synthetic.main.fragment_two.*
import kotlinx.android.synthetic.main.item_schedule.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.exp

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [two_fragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [two_fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class two_fragment : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_two, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val schedule = ArrayList<Schedule>()
        schedule.add(Schedule("07.00-10.00","Matematika",""))
        schedule.add(Schedule("10.00-12.00","Bahasa Indonesia","•  PR dan tugas dikumpulkan hari ini\n" +
                "•  Diajar oleh guru tamu\n" +
                "•  Mulai masuk materi baru"))
        schedule.add(Schedule("12.00-1.00","Sejarah",""))
        schedule.add(Schedule("01.00-02.00","Istirahat",""))
        schedule.add(Schedule("02.00-04.00","Algoritma",""))

        val calender = Calendar.getInstance()
        val inFormat = SimpleDateFormat("EEEE, dd/MM/yyyy")
        tvdate.setText(inFormat.format(calender.time))

        val adapter = ScheduleAdapter(schedule)

        val lineardate = activity.findViewById<View>(R.id.linearDate) as? LinearLayout
        collaps(lineardate!!)

        listSchedule.layoutManager = LinearLayoutManager(activity,LinearLayout.VERTICAL,false)
        listSchedule.adapter = adapter

        linearDesc.setOnClickListener {
            expand(lineardate!!)
            val animation = AnimationUtils.loadAnimation(activity,R.anim.circular)
            img_date.animate().rotation(180f).start()
            linearDesc.isEnabled = false

        }
        
        calenderSchedule.setOnDateChangeListener { calendarView, year, month, dayOfMonth -> val date = "$dayOfMonth / ${month+1} / $year"
            val cal = Calendar.getInstance()
            cal.set(year,month,dayOfMonth)
            val dateOfDay = cal.get(Calendar.DAY_OF_WEEK)
            var nameOfDay = ""
            when (dateOfDay){
                1-> nameOfDay = "Minggu"
                2-> nameOfDay = "Senin"
                3-> nameOfDay = "Selasa"
                4-> nameOfDay = "Rabu"
                5-> nameOfDay = "Kamis"
                6-> nameOfDay = "Jum'at"
                7-> nameOfDay = "Sabtu"
            }
            tvdate.setText("${nameOfDay}, " +date)
            img_date.animate().rotationBy(180f).start()
            collaps(lineardate!!)
            linearDesc.isEnabled = true
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment two_fragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): two_fragment {
            val fragment = two_fragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    fun expand(v: View){
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
        val targetHeight: Int = v.measuredHeight

        v.layoutParams.height = 1
        v.visibility = View.VISIBLE

        val a: Animation = object : Animation(){
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    LinearLayout.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()

            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

    fun collapse(v: View) {
        val initialHeight : Int = v.measuredHeight
        val a : Animation = object : Animation(){
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                if (interpolatedTime == 1f){
                    v.visibility = View.GONE
                }else{
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }
}// Required empty public constructor

package com.example.gredu.myapplication.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.model.DataPelajaran
import kotlinx.android.synthetic.main.item_perform.view.*

/**
 * Created by gredu on 28/11/17.
 */
class PelajaranAdapter(val matPel: ArrayList<DataPelajaran>, val context: Context): RecyclerView.Adapter<PelajaranAdapter.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.pelajaran.setText(matPel[position].pelajaran)
        holder.nilaiPelajaran.setText("${matPel[position].nilai}")
        if (matPel[position].nilai >= 90){
            holder.nilaiPelajaran.setTextColor(context.resources.getColor(R.color.colorText100))
        }else if (matPel[position].nilai >= 80){
            holder.nilaiPelajaran.setTextColor(context.resources.getColor(R.color.colorText80))
        }else if (matPel[position].nilai >= 70){
            holder.nilaiPelajaran.setTextColor(context.resources.getColor(R.color.colorText70))
        }else if (matPel[position].nilai >= 60){
            holder.nilaiPelajaran.setTextColor(context.resources.getColor(R.color.colorText60))
        }else if (matPel[position].nilai >= 55 || matPel[position].nilai <= 55){
            holder.nilaiPelajaran.setTextColor(context.resources.getColor(R.color.colorText55))
        }
        holder.nextPerform.setOnClickListener {
            Toast.makeText(context,"Next Detail",Toast.LENGTH_SHORT).show()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder{
        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.item_perform,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return matPel.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val pelajaran = itemView.tvNamePelajaran
        val nilaiPelajaran = itemView.tvValuePelajaran
        val nextPerform = itemView.img_next
    }
}
package com.example.gredu.myapplication.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.model.User
import kotlinx.android.synthetic.main.list_user.view.*
import java.util.*

/**
 * Created by gredu on 23/11/17.
 */
class UserAdapter(val user: ArrayList<User>) : RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: UserAdapter.ViewHolder, position: Int) {
        holder.name.setText(user[position].name)
        holder.password.setText(user[position].password)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_user,parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return user.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){

        val name = itemView.tvUserName
        val password = itemView.tvPassword

        /*fun bindItems(user: User){
            val username = itemView.findViewById<TextView>(R.id.tvUserName) as TextView
            val password = itemView.findViewById<TextView>(R.id.tvPassword) as TextView

            username.text = user.name
            password.text = user.password
        }*/

    }
}




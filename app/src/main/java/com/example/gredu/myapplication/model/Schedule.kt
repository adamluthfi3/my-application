package com.example.gredu.myapplication.model

/**
 * Created by gredu on 05/12/17.
 */
data class Schedule (val time: String, val course: String, val description: String)
package com.example.gredu.myapplication.fragment

import android.app.ActionBar
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.Transformation
import android.widget.*
import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.adapter.PelajaranAdapter
import com.example.gredu.myapplication.model.DataPelajaran
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [one_fragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [one_fragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class one_fragment : Fragment(), AccountItemClick{

    override fun onAccountItemClick() {
        val snackBar = Snackbar.make(view!!,"account change",Snackbar.LENGTH_SHORT)
        snackBar.show()
    }

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    private var mListener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments!!.getString(ARG_PARAM1)
            mParam2 = arguments!!.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        setHasOptionsMenu(true)
        val v = inflater.inflate(R.layout.fragment_one,container,false)
        val data = ArrayList<DataPelajaran>()
        data.add(DataPelajaran("Indonesia",97))
        data.add(DataPelajaran("Matematika",87))
        data.add(DataPelajaran("English",74))
        data.add(DataPelajaran("Fisika",63))
        data.add(DataPelajaran("Kimia",55))
        data.add(DataPelajaran("Biologi",70))
        data.add(DataPelajaran("Sosiologi",40))
        data.add(DataPelajaran("Ekonomi",80))
        data.add(DataPelajaran("Algoritma",78))
        data.add(DataPelajaran("Project Manajemen",58))

        val adapterPelajaran = PelajaranAdapter(data,v.context)
        val list = v.findViewById<View>(R.id.list_perform) as? RecyclerView
        val textValueAbsensi = v.findViewById<View>(R.id.tvValueAbsensi) as? TextView
        val linearAbsensi = v.findViewById<View>(R.id.linear_absensi) as? LinearLayout
        val swicthAccount = v.findViewById<View>(R.id.img_swich_account) as? ImageView

        collapse(linearAbsensi!!)

        textValueAbsensi!!.setOnClickListener {
            expand(linearAbsensi!!)
        }

        linearAbsensi!!.setOnClickListener {
            collapse(linearAbsensi)
        }

        swicthAccount!!.setOnClickListener {
            val switch = account_fragment()
            switch.show(fragmentManager,switch.tag)
        }

        list!!.layoutManager = LinearLayoutManager(v.context,LinearLayout.VERTICAL,false)
        list.adapter = adapterPelajaran

        return v
    }

    // TODO: Rename method, update argument and hook method into UI event
    fun onButtonPressed(uri: Uri) {
        if (mListener != null) {
            mListener!!.onFragmentInteraction(uri)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     *
     *
     * See the Android Training lesson [Communicating with Other Fragments](http://developer.android.com/training/basics/fragments/communicating.html) for more information.
     */
    interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        fun onFragmentInteraction(uri: Uri)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment one_fragment.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): one_fragment {
            val fragment = one_fragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.findItem(R.menu.menu_fragment_one).setVisible(true)
        inflater!!.inflate(R.menu.menu_fragment_one,menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.item_message -> Toast.makeText(activity,"Pesan",Toast.LENGTH_SHORT).show()
        }
        return super.onOptionsItemSelected(item)
    }

    fun expand(v: View){
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
        val targetHeight: Int = v.measuredHeight

        v.layoutParams.height = 1
        v.visibility = View.VISIBLE

        val a: Animation = object : Animation(){
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                v.layoutParams.height = if (interpolatedTime == 1f)
                    LinearLayout.LayoutParams.WRAP_CONTENT
                else
                    (targetHeight * interpolatedTime).toInt()
                v.requestLayout()

            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

    fun collapse(v: View) {
        val initialHeight : Int = v.measuredHeight
        val a : Animation = object : Animation(){
            override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                if (interpolatedTime == 1f){
                    v.visibility = View.GONE
                }else{
                    v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                    v.requestLayout()
                }
            }

            override fun willChangeBounds(): Boolean {
                return true
            }
        }

        a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
        v.startAnimation(a)
    }

}// Required empty public constructor

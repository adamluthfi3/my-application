package com.example.gredu.myapplication.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.model.Schedule
import kotlinx.android.synthetic.main.item_schedule.view.*

/**
 * Created by gredu on 05/12/17.
 */
class ScheduleAdapter (val schedule: ArrayList<Schedule>): RecyclerView.Adapter<ScheduleAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder{
        val v = LayoutInflater.from(parent!!.context).inflate(R.layout.item_schedule,parent,false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder!!.textTime.setText(schedule[position].time)
        holder!!.textCourse.setText(schedule[position].course)
        holder!!.textDesc.setText(schedule[position].description)
    }

    override fun getItemCount(): Int {
        return schedule.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val textTime = itemView.tvTime
        val textCourse = itemView.tvCourse
        val textDesc = itemView.tvDescription
    }
}
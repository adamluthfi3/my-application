package com.example.gredu.myapplication.model

/**
 * Created by gredu on 28/11/17.
 */
data class DataPelajaran(val pelajaran: String, val nilai: Int)
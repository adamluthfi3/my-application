package com.example.gredu.myapplication

import android.net.Uri
import android.os.Build
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View

import com.example.gredu.myapplication.fragment.one_fragment
import com.example.gredu.myapplication.fragment.thrid_fragment
import com.example.gredu.myapplication.fragment.two_fragment

class Main2Activity : AppCompatActivity(), one_fragment.OnFragmentInteractionListener,
        two_fragment.OnFragmentInteractionListener,thrid_fragment.OnFragmentInteractionListener{

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main2)
        val toolbar = findViewById<View>(R.id.toolbar_fragment_one) as? Toolbar
        setSupportActionBar(toolbar)

        val bottomNavigationView = findViewById<View>(R.id.navigation_bottom) as BottomNavigationView

        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            var selectedFragment: Fragment? = null
            when (item.itemId) {
                R.id.action_item_1 -> selectedFragment = one_fragment.newInstance("","")
                R.id.action_item_2 -> selectedFragment = two_fragment.newInstance("","")
                R.id.action_item_3 -> selectedFragment = thrid_fragment.newInstance("","")
            }
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.container, selectedFragment)
            transaction.commit()
            true
        }

        //Manually displaying the first fragment - one time only
        val transaction = supportFragmentManager.beginTransaction()
        val oneFragment = one_fragment()
        transaction.replace(R.id.container, oneFragment).addToBackStack("tag")
        transaction.commit()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        val id = item!!.itemId

        if  (id == R.id.action_settings){

        }
        return super.onOptionsItemSelected(item)
    }
}

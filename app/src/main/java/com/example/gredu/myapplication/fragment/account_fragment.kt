package com.example.gredu.myapplication.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.example.gredu.myapplication.R
import com.example.gredu.myapplication.adapter.AccountAdapter
import com.example.gredu.myapplication.adapter.PelajaranAdapter
import com.example.gredu.myapplication.model.Account
import com.example.gredu.myapplication.model.DataPelajaran

/**
 * Created by gredu on 30/11/17.
 */

interface AccountItemClick {
    fun onAccountItemClick()
}

class account_fragment : BottomSheetDialogFragment() {

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog?, style: Int) {
        super.setupDialog(dialog, style)
        val contentView: View = View.inflate(context, R.layout.bottom_sheet_account, null)
        val listAccount = ArrayList<Account>()
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Milea Hermansyah","SMA 7 Jakarta - X IPA 1"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Chelsea Hermansyah","SMA 9 Jakarta - XI IPA 4"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Rudi Hermansyah","SMA 8 Jakarta - IX IPA 3"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Dedi Hermansyah","SMA 5 Jakarta - X IPA 9"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Gunanjar Hermansyah","SMA 1 Jakarta - X IPA 2"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Hasan Hermansyah","SMA 2 Jakarta - XI IPA 7"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Prince Hermansyah","SMP 4 Jakarta - VII IPA 6"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Kasino Hermansyah","SMA 5 Jakarta - XII IPA 3"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Shomad Hermansyah","SMP 1 Jakarta - VIII IPA 6"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Kevin Hermansyah","SMA 2 Jakarta - X IPA 5"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Dani Hermansyah","SMA 4 Jakarta - XI IPA 2"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Herman Hermansyah","SD 5 Jakarta"))
        listAccount.add(Account(R.mipmap.ic_launcher_round,"Reza Hermansyah","SMA 3 Jakarta - XI IPA 3"))


        val adapter = AccountAdapter(listAccount,contentView.context,object : AccountItemClick{
            override fun onAccountItemClick() {
                val snackbar = Snackbar.make(contentView,"account change",Snackbar.LENGTH_SHORT)
                snackbar.show()
                dismiss()
            }
        })
        val list_account = contentView.findViewById<View>(R.id.recyclerAccount) as? RecyclerView
        val close = contentView.findViewById<View>(R.id.img_close) as? ImageView

        list_account!!.layoutManager = LinearLayoutManager(contentView.context,LinearLayout.VERTICAL,false)
        list_account.adapter = adapter

        close!!.setOnClickListener {
            dismiss()
        }

        dialog!!.setContentView(contentView)
    }

}
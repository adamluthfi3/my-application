package com.example.gredu.myapplication.adapter

import android.view.View
import android.view.animation.Animation
import android.view.animation.Transformation

/**
 * Created by gredu on 06/12/17.
 */
fun collaps(v:View){
    val initialHeight : Int = v.measuredHeight
    val a : Animation = object : Animation(){
        override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
            if (interpolatedTime == 1f){
                v.visibility = View.GONE
            }else{
                v.layoutParams.height = initialHeight - (initialHeight * interpolatedTime).toInt()
                v.requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }

    a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
    v.startAnimation(a)
}
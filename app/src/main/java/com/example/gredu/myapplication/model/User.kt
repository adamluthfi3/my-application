package com.example.gredu.myapplication.model

/**
 * Created by gredu on 23/11/17.
 */
data class User(val name: String, val password: String)